import { Component, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTable } from '@angular/material/table';

interface IPessoa {
  codigo: number;
  nome: string;
  telefone: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  form: FormGroup;
  @ViewChild(MatTable) matTable: MatTable<any> | undefined;
  @ViewChild(FormGroupDirective) ngForm: FormGroupDirective | undefined;
  data: IPessoa[] = [];
  filteredData: IPessoa[] = [];
  tableColumns = ['codigo', 'nome', 'telefone', 'apagar'];
  filter: string = '';
  constructor(private fb: FormBuilder, private snackBar: MatSnackBar) {
    this.form = this.fb.group({
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
    });
    this.filteredData = this.data;
  }

  cleanForm(formDirective: FormGroupDirective) {
    this.form.reset();
    formDirective.resetForm();
  }

  clearFilter() {
    this.filter = '';
    this.onFilter();
  }
  onFilter() {
    if (this.filter.length > 0)
      this.filteredData = this.data.filter(
        (p) =>
          p.codigo == +this.filter ||
          p.nome.toLowerCase().includes(this.filter.toLowerCase()) ||
          p.telefone.includes(this.filter)
      );
    else this.filteredData = this.data;
    this.matTable?.renderRows();
  }

  submit(formDirective: FormGroupDirective) {
    if (this.form.invalid) {
      this.snackBar.open(' Formulário inválido', undefined, {
        verticalPosition: 'top',
      });
      return;
    }
    let lastCode =
      this.data.length == 0 ? 0 : this.data[this.data.length - 1].codigo;

    this.data.push({
      codigo: lastCode + 1,
      nome: this.form.get('nome')?.value,
      telefone: this.form.get('telefone')?.value,
    });
    this.filteredData = this.data;
    this.matTable?.renderRows();
    this.cleanForm(formDirective);
  }

  deletePessoa(pessoa: IPessoa) {
    let indexOf: number = this.data.indexOf(pessoa);
    if (indexOf >= 0) {
      this.data.splice(indexOf, 1);
      this.matTable?.renderRows();
    }
  }

  sort(event: any) {
    switch (event.active) {
      case 'nome':
        this.data.sort((a: IPessoa, b: IPessoa) =>
          event.direction == 'asc'
            ? a.nome.localeCompare(b.nome)
            : b.nome.localeCompare(a.nome)
        );
        break;
      case 'codigo':
        this.data.sort((a: IPessoa, b: IPessoa) =>
          event.direction == 'asc' ? a.codigo - b.codigo : b.codigo - a.codigo
        );
        break;
      case 'telefone':
        this.data.sort((a: IPessoa, b: IPessoa) =>
          event.direction == 'asc'
            ? a.telefone.localeCompare(b.telefone)
            : b.telefone.localeCompare(a.telefone)
        );
        break;
    }
    this.matTable?.renderRows();
  }
}
