# Prova Angular para Vaga na Target Sistemas

Este sistema é a resposta ao teste de Angular para a vaga de Desenvolvedor junto à Target Sistemas

## Rodando o sistema

Para rodar o sistema, bastar clonar o repositório, executar o comando `npm i` para instalar as dependências e após o término executar o comando `ng serve`

## Sobre o sistema

O sistema utiliza o Angular 13 juntamente com a versão correspondente do Angular Material.
O software consiste em uma página com dois componentes principais: o primeiro um card com formulário onde é possível ao usuário entrar com os dados de nome e telefone. O segundo componente é uma tabela onde os dados salvos são exibidos.
É possível excluir determinado registro, filtrar, ordenar e limpar o formulário.
# Contato

Para contato com o desenvolvedor do sistema basta enviar um email para ednildo@outlook.com.br ou nildo.777@gmail.com
